Welcome to my personal website repository. The site is live at [nparvez.gitlab.io](https://nparvez.gitlab.io). Please feel free to fork and customize as you wish. 

This website is built using `Researcher` theme for Jekyll by Ankita Sultana. See the demo website here:: [http://ankitsultana.com/researcher](http://ankitsultana.com/researcher). The original source code and a basic tutorial is available here: [https://github.com/ankitsultana/researcher](https://github.com/ankitsultana/researcher)

### License

[GNU GPL v3](https://github.com/bk2dcradle/researcher/blob/gh-pages/LICENSE)
