---
layout: default
is_blog: true
permalink: /blog/
---
Here you will find some recent updates, helpful guides, and some miscellaneous things I care about.

Recent Updates
---

- (Jan 2024) Our paper on the methodology and numerical convergence of fiber network simulations has been selected as the cover of [12th issue of Fiber](https://www.mdpi.com/2079-6439/12/1)!

- (Dec 2023) I am honored to accept the [Belsky award](<https://giving.rpi.edu/belsky-award/>) for computational sciences at RPI!

Common Tools
---

- **Progamming editor**: [VS code](https://code.visualstudio.com/) for most purposes, [NeoVIM](https://neovim.io/) for terminals.
- **Note taking**: Markdown notetaking with [Obsidian](https://obsidian.md/).
- **Console enhancement**: Try [ble.sh](https://github.com/akinomyoga/ble.sh) to color the aging `bash` of the institutional computer system. It comes with autocompletions as well, though not as fast as alternatives. Otherwise, there is nothing like [oh-my-zsh](https://ohmyz.sh/)! There is also [oh-my-bash](https://ohmybash.nntoan.com/) for `bash` but with limited features. 
- **Document preparation**: Use LateX whenever possible.

Misc
---

Please consider donating to public platforms and computational tools that are used daily by many people like me. Your donation can make a difference.

Here are some well-known projects that can benefit from your contributions: [WIKIMEDIA](https://donate.wikimedia.org/), [NumFOCUS](https://numfocus.org/donate-to-numpy), [Software freedom conservancy](https://sfconservancy.org/).

---
<span style="font-size:0.5em;">Last Updated: May, 2024 | Powered by Ankit Sultana's [Researcher](https://github.com/ankitsultana/researcher) theme</span>
