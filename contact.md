---
layout: default
permalink: /contact/
---
I am currently in Troy, NY. The best way to contact me is through **email** or **WhatsApp** (if you are on my contact list). I am also available on LinkedIn.

Email
---

[parvem@rpi.edu](mailto:parvem@rpi.edu)

External Profile Links
---

[LinkedIn](https://www.linkedin.com/in/nishanparvez/)

[Github](https://github.com/npdroid)

---
<span style="font-size:0.5em;">Last Updated: Jan, 2024 | Powered by Ankit Sultana's [Researcher](https://github.com/ankitsultana/researcher) theme</span>
