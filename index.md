---
layout: default
---

## About Me

<img class="profile-picture" src="meeh.png">

Hello, and welcome to my homepage! I am a Ph.D. candidate in Mechanical Engineering at [Rensselaer Polytechnic Institute](https://mane.rpi.edu/). My work focuses on computational analysis of [fibrous network materials](https://en.wikipedia.org/wiki/Fiber_network_mechanics) (e.g., soft tissue, paper, hydrogels, etc.) under the guidance of [Dr. Catalin Picu](https://scorec.rpi.edu/nanomechanics/). The up-to-date list of my published works is available at [google scholar](https://scholar.google.com/citations?user=_P36YWkAAAAJ&hl=en).

Please feel free to browse around or [contact me](/contact/) for questions related to my work!

## Ongoing Research Projects: Large deformation and damage in soft fibrous materials.

<img class="![profile](sample.png)-picture" src="sample.png" width=600px>



**Constitutive modeling of fibrous materials using neural network:**
Fiber network materials are thought to obey hyperelastic constitutive law formulation. This is undoubtedly the case when the network is embedded in a matrix, e.g., biological tissue or hydrogel. The dry network, on the other hand, shows kinematic deviations from continuum predictions. Thus, general predictions about their mechanical behavior often require discrete numerical simulations. This project aims to develop a [machine learning-based model](http://arxiv.org/abs/2107.05388) to circumvent such expensive simulations. In particular, the model comprises of [Input Convex Neural Network](http://arxiv.org/abs/1609.07152) with [Sobolev space training](https://arxiv.org/abs/1706.04859) so that the resulting model is convex and not only the energy but the stress and tangent stiffness data are used during training as well. The model is purpose-built to replace the microscale part of a massively parallel GPU-based [multiscale simulation code](https://arxiv.org/abs/2306.09427). As such, mechanical problems involving tissue material can be solved on your laptop instead of your institution's supercomputer!

**Mechanism of damage and fracture in network materials with pre-existing cracks:** Fibrous materials exhibit [insenstivity to pre-existing crack](10.1016/j.engfracmech.2018.11.012). This observation has been linked to material parameters such as the distribution of porosity and material density. At the same time, the size effect has been shown to control a transition from [brittle to ductile failure](https://arxiv.org/abs/2309.00279). The intrinsic material parameters for unnotched samples can also control the [strength and failure behavior](10.1016/j.jmps.2018.03.026). Finally, in the background is the added complexity of [stochastic failure](https://doi.org/10.1039/C2SM25450F) of the fibers. This project aims to bring these observations together to identify the kinematics of the deformation near the crack tip and the parameters or processes that control the critical crack size. The implication lies in biology (e.g., rupture of blood clots, tendons, etc.) and in defining the scope of engineering applications of fibrous materials.

**The effect of material parameters and pressure differential on aortic dissection:** [Aorta](https://en.wikipedia.org/wiki/Aorta) is the main blood vessel in the human body that carries blood from the heart. Structurally, this is a tube of multiple layers, each composed of fibrous material (collagen, elastic) and soft muscle cells of different properties. The overall material behavior is often described by hyperelastic constitutive models such as [Holzapfel-Gasser-Ogden](https://link.springer.com/chapter/10.1007/0-306-48389-0_1). Structural damage in the aorta can lead to various complexities, including death. In particular, traumatic injury (from motor vehicle accidents, for example) can cause aortic dissection and rupture, leading to acute internal bleeding. In collaboration with [Dr. Holzapfel](https://www.biomech.tugraz.at/people/gerhard_holzapfel), here we aim to identify the incipient crack growth mechanism in descending human aorta and the effect of various physiological parameters on this crack growth mechanism.  

## Recent Publications

- Methodological Aspects and Mesh Convergence in Numerical Analysis of Athermal Fiber Network Material Deformation; *N Parvez*, SN Amjad, MK Dey, CR Picu, [Fibers, 2024](https://doi.org/10.3390/fib12010009)
- Stiffening mechanisms in stochastic athermal fiber networks; *N Parvez*, J Merson, RC Picu, [Physical Review E, 2023](https://doi.org/10.1103/PhysRevE.108.044502)
- Probing soft fibrous materials by indentation; J Merson, *N Parvez*, RC Picu
[Acta Biomaterialia, 2023](https://doi.org/10.1016/j.actbio.2022.03.053)
- Effect of connectivity on the elasticity of athermal network materials
*N Parvez*, CR Picu, [Soft Matter, 2023](https://doi.org/10.1039/D2SM01303G)
- ... more at [google scholar](https://scholar.google.com/citations?user=_P36YWkAAAAJ&hl=en)

---
<span style="font-size:0.5em;">Last Updated: Jan, 2024 | Powered by Ankit Sultana's [Researcher](https://github.com/ankitsultana/researcher) theme</span>
