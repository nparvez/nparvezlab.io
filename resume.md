---
layout: default
is_resume: true
permalink: /resume/
---
My full CV is available here: [nishan_parvez_cv.pdf](/nishan_parvez_cv.pdf).

Education
---

- PhD in Mechanical Engineering, [RPI](https://www.rpi.edu/), Troy, NY [2020-2024]
- MSc in Mechanical Engieering, [Miami University](https://miamioh.edu/index.html), Oxford, OH [2018-2020]
- Bsc in Mechanical Engineering, [BUET](https://www.buet.ac.bd/), Dhaka, Bangladesh [2013-2017]

Recent Publications
---

- Methodological Aspects and Mesh Convergence in Numerical Analysis of Athermal Fiber Network Material Deformation; *N Parvez*, SN Amjad, MK Dey, CR Picu, [Fibers, 2024](https://doi.org/10.3390/fib12010009)
- Stiffening mechanisms in stochastic athermal fiber networks; *N Parvez*, J Merson, RC Picu, [Physical Review E, 2023](https://doi.org/10.1103/PhysRevE.108.044502)
- Probing soft fibrous materials by indentation; J Merson, *N Parvez*, RC Picu
[Acta Biomaterialia, 2023](https://doi.org/10.1016/j.actbio.2022.03.053)
- Effect of connectivity on the elasticity of athermal network materials
*N Parvez*, CR Picu, [Soft Matter, 2023](https://doi.org/10.1039/D2SM01303G)

Experiences
---

- **Research:** Numerical investigation into the deformation and damage of soft fibrous materials using computational tools such as finite element analysis, machine learning, molecular dynamics, and numerical analysis.
- **Teaching:** Teaching assistant in senior undergraduate courses such as Mechanical Behavior of Materials, Control Theory, and Heat Transfer.

Awards
---

- [Belsky award](https://giving.rpi.edu/belsky-award/) for computational science and engineering at RPI [2023]
- [Graduate Student's Achievement Award](https://miamioh.edu/graduate-school/admission-funding/funding-awards/grad-student-achievement-award.html) at Miami University [2019]

---
<span style="font-size:0.5em;">Last Updated: Jan, 2024 | Powered by Ankit Sultana's [Researcher](https://github.com/ankitsultana/researcher) theme</span>
